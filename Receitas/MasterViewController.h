//
//  MasterViewController.h
//  Receitas
//
//  Created by Jonas Rotilli on 23/09/13.
//  Copyright (c) 2013 Jonas Rotilli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;

@end
