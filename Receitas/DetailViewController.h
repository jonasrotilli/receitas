//
//  DetailViewController.h
//  Receitas
//
//  Created by Jonas Rotilli on 23/09/13.
//  Copyright (c) 2013 Jonas Rotilli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
