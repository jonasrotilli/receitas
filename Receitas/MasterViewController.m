//
//  MasterViewController.m
//  Lista
//
//  Created by Jonas Rotilli on 19/09/13.
//  Copyright (c) 2013 Jonas Rotilli. All rights reserved.
//

#import "MasterViewController.h"

#import "DetailViewController.h"

@interface MasterViewController () {
    NSMutableArray* _objects;
    NSMutableArray* _receitas;
}
@end

@implementation MasterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Lista de Receitas", @"Master");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    //UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    //self.navigationItem.rightBarButtonItem = addButton;
    
    _objects = [[NSMutableArray alloc] init];
    [_objects insertObject:@"Strogonoff de Carne" atIndex:0];
    [_objects insertObject:@"Bolinho de Chuva" atIndex:1];
    [_objects insertObject:@"Sobremesa de Ovo frito" atIndex:2];
    
    _receitas = [[NSMutableArray alloc] init];
    [_receitas insertObject:@"Strogonoff de Carne - Receita \n \n 1 - Corte a carne selecionada em tirinhas, tempere com a pimenta, cominho e sal \n 2 - Deixe descansar a carne por 20 minutos para apurar o gosto \n 3 - Coloque óleo suficiente para fritar poucas quantidades de tirinhas (7 ou 8)retire as tiras e reserve \n 4 - Não deixe fritar muito para que a carne não fique dura." atIndex:0];
    [_receitas insertObject:@"Bolinho de Chuva - Receita \n \n 1 - Misture todos os ingredientes até ficar uma massa não muito mole, nem tão dura \n 2 - Deixe aquecer uma panela com bastante óleo para que os bolinhos possam boiar \n 3 - Quando estiver bem quente comece a colocar colheradas da massa e abaixe o fogo para que o bolinho não fique crú por dentro \n 4 - Coloque os bolinhos sobre papel absorvente e depois se preferir passe-os no açúcar com canela." atIndex:1];
    [_receitas insertObject:@"Sobremesa de Ovo frito - Receita \n \n 1 - Leve ao fogo o leite, o leite condensado, o leite de coco e o amido de milho\n 2 - Mexa até engrossar, deixe esfriar \n 3 - Coloque um pouco desse creme sobre um prato, espalhando de forma que se pareça com clara frita\n 4 - Peque metade de um pêssego e coloque virado para baixo sobre o creme\n 5 - Espalhe um pouco de calda para parecer óleo gelado." atIndex:2];
    
    //[self insertNewObject:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
        /*
         [_objects insertObject:@"Molho de alho" atIndex:0];
         [_objects insertObject:@"Molho Vermelho" atIndex:0];
         [_objects insertObject:@"Ovo frito" atIndex:0];
         */
        
    }
    [_objects insertObject:@"Receita adicional" atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _objects.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    
    NSDate *object = _objects[indexPath.row];
    cell.textLabel.text = [object description];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.detailViewController) {
        self.detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
    }
    NSString *object = _receitas[indexPath.row];
    self.detailViewController.detailItem = object;
    [self.navigationController pushViewController:self.detailViewController animated:YES];
}

@end
