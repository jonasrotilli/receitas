//
//  AppDelegate.h
//  Receitas
//
//  Created by Jonas Rotilli on 23/09/13.
//  Copyright (c) 2013 Jonas Rotilli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;

@end
